// Let's do something dangerous
if (!String.format) {
  String.format = function(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number] 
        : match
      ;
    });
  };
}


function getDefault(x_str){
  switch(x_str){
    case 'language':
      return 'fr';
      break;
    case 'hasLoggedAlready':
      return 'no';
      break;
    case 'dictObj':
      return {};
      break;
    case 'dictObjVersion':
      return 0;
      break;

    default:
      return null;
  }
}

// Returns if a value is an object
function isObject (value) {
return value && typeof value === 'object' && value.constructor === Object;
};

function getLocal(x_str, x_asObject){
  var toReturn =  localStorage[x_str] === undefined ? getDefault(x_str) : localStorage[x_str];
  if(x_asObject){
    return JSON.parse(toReturn);
  }
  else 
    return toReturn;

}


function setLocal(x_key, x_value){
  if(isObject(x_value)){    
    x_value = JSON.stringify(x_value, null,2);
  }
  localStorage.setItem(x_key,x_value);
}

// This function is only a vanilla wrapper for convenience
function gord(x_str){
  return getOrReturnDefault(x_str);
}


// This returns a string in a very lightweight and readable way
function _s() { 
  return String.format.apply(null,arguments);
}

// This echoes with a string format approach
function __(){
  console.log(_s.apply(null,arguments));
}
