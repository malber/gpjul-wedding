

function init() {
	load_navbar();
	showLoadingModal(1);
	redirectIfShortcut();
	attachCallbacks();
	translateInit();
	sharedInit(); 
	// This for full load
	// translateInitFromCache();
}

function redirectIfShortcut() {
	if(getLocal('hasLoggedAlready') == 'no'){ // on purpose, unfortunately
		window.location.href = "access.html";
	} else {
		// Do nothing
	}
}

function attachCallbacks(){
	// put callbacks here
}

function switchLanguage(){ 
	showLoadingModal(2);
	var otherLanguage = getOtherLanguage();
	translateTo(otherLanguage);
	setLocal('language',otherLanguage)
}
