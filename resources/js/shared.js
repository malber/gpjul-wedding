
var config = {
	'language' : null,
	'hasLoggedAlready' : false,
	'dictObj' : {},
	'languages' : []
};

var dictObj = {};

function sharedInit(){
	// Nothing for now

}
function load_navbar(){
	$('#shared-part-header').load('shared/header.html',refreshTranslate);
	$('#shared-part-footer').load('shared/footer.html',refreshTranslate);
}

function refreshTranslate(){

	translateTo(getLocal('language'));

}
function hasLoggedAlready() {
	return getLocal('hasLoggedAlready') == "yes";
}


function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function buildDictionaryObjectFromSqlLite(x_contents, x_version){
	// header
	var version = x_version[0].values[0];

	var languagesrow = x_contents[0].columns;
	var languages = [];
	var dictObj = {};

	for(var i=1;i<languagesrow.length;i++){
		languages.push(languagesrow[i].trim());
	}


	for(var line=0;line < x_contents[0].values.length ; line++){
		var line_content = x_contents[0].values[line];
		dictObj[line_content[0]] = {};

		for(var i=1;i<line_content.length;i++){
			dictObj[line_content[0]][languages[i - 1]] = line_content[i].trim();
		}
	}

	setLocal('dictObj', dictObj);
	setLocal('dictObjVersion', version);
	setLocal('languages', languages);
}

function buildDictionaryObjectFromCSV(x_contents, x_version){
	// header

	var languagesrow = x_contents[0];
	var languages = [];
	var dictObj = {};

	for(var i=1;i<languagesrow.length;i++){
		languages.push(languagesrow[i].trim());
	}


	for(var line=1;line < x_contents.length ; line++){

		var line_content = x_contents[line];
		dictObj[line_content[0]] = {};

		for(var i=1;i<line_content.length;i++){
			dictObj[line_content[0]][languages[i - 1]] = line_content[i].trim();
		}
	}

	setLocal('dictObj', dictObj);
	setLocal('dictObjVersion', x_version);
	setLocal('languages', languages);
}

function translateTo(x_lang){
	var customAttribute = "data-custom-lang";
	$(_s('[{0}]',customAttribute)).each(function(){
		var dictEntry = $(this).attr(customAttribute);
		try {
			this.innerHTML = (dictObj[dictEntry][x_lang]).autoLink({class:"link", target: "_blank", rel: "nofollow"});
		} catch (err) {
			__("Issue with {0}, {1}", dictEntry, err );
		}
	});
}


function loadCachedTranslationDB(){
	dictObj = getLocal('dictObj',true);
	translateTo(getLocal('language'));
}
function translateInitFromCache(){
	loadCachedTranslationDB();
}


function loadTranslationDBFromSQLite(){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'resources/dict/language.db?v=' + Math.random(), true);
	xhr.responseType = 'arraybuffer';

	xhr.onload = function(e) {
	  var uInt8Array = new Uint8Array(this.response);
	  var db = new SQL.Database(uInt8Array);
	  var contents = db.exec("SELECT * FROM Translation");
	  // contents is now [{columns:['col1','col2',...], values:[[first row], [second row], ...]}]
	  var version = db.exec("SELECT * FROM Version ORDER BY number DESC");
	  buildDictionaryObjectFromSqlLite(contents, version);
	};
	xhr.send();
}

function loadTranslationDBFromCSV(){
	// Parse local CSV file
	Papa.parse("./resources/dict/dict.csv?version=" + Math.random(), {
		complete: function(results) {
			_dict = results;
			buildDictionaryObjectFromCSV(_dict.data, 1 ); // forcing version 1
		},
		download:true,
		quoteChar: '"',
		delimiter: ",",
		// header: true
	});

}

function isLanguageAllowed(x_lang){

	var allLang = ['it','fr'];
	return allLang.indexOf(x_lang) > -1;
}

function translateInit(){
	// loadTranslationDBFromSQLite();
	loadTranslationDBFromCSV();
	dictObj = getLocal('dictObj',true);

	try {
	var strings = getUrlVars();
		if('lang' in strings && isLanguageAllowed(strings['lang'])){
			setLocal('language',strings['lang']);
		}
	}catch(err){
		console.log('error getting default language from URL: ' + err);
	}

	//
	translateTo(getLocal('language'));

}


function showLoadingModal(x_seconds){
	$('#loadingModal').show();
	setTimeout(hideLoadingModal, x_seconds*1000);
}

function hideLoadingModal(){
	$('#loadingModal').hide();
}

function showAccessModal(){
	$('#accessModal').show();
}

function hideAccessModal(){
	$('#accessModal').hide();
}

function getOtherLanguage(){
	switch(getLocal('language')){
    case 'it':
      return 'fr';
      break;
    case 'fr':
      return 'it';
      break;
    default:
      alert(' -- -- --');
      return null;
  }

}
