
function checkCode(){
	try {
		var code = $('#accessModal input').val();
		if(code.toLowerCase() == 'gpjul') {
			setLocal('hasLoggedAlready','yes');
			window.location.href = "./";
			return;
		} else {
			alert('error! You entered: ' + code);
		}
	}
	catch ( err ){ 
		// do nothing dude
	}


}

function forwardIfAlreadyLogged(){
	if(hasLoggedAlready()){ // on purpose, unfortunately
		window.location.href = "./";
	} else {
		// Do nothing
	}
	return;
}

function init() {
	forwardIfAlreadyLogged();
	load_navbar();
	showAccessModal();
	attachCallbacks();
	setTimeout(loadTranslationDB, 1000);
	sharedInit();
	
}

function attachCallbacks() {
	$('#code-check-button').click(function(){
		checkCode();
	});
}



